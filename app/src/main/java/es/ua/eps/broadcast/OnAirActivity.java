package es.ua.eps.broadcast;

import net.majorkernelpanic.streaming.Session;
import net.majorkernelpanic.streaming.Session.Callback;
import net.majorkernelpanic.streaming.SessionBuilder;
import net.majorkernelpanic.streaming.audio.AudioQuality;
import net.majorkernelpanic.streaming.gl.SurfaceView;
import net.majorkernelpanic.streaming.rtsp.RtspClient;
import net.majorkernelpanic.streaming.video.VideoQuality;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;

public class OnAirActivity extends Activity implements SurfaceHolder.Callback, Session.Callback, RtspClient.Callback{

	static String DEBUG = "#DEBUG:";

	//Config broadcast user permissions
	static String USER = "publisher";
	static String PASS = "mastermoviles";
	
	SurfaceView mSurfaceView;
	SurfaceHolder mHolder;
	Session mSession;
	RtspClient mRTSPClient;

	private Button btIniciar, btSalir;
	
	String ip;
	int channel;
	int quality;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_streaming);
				
		ip = this.getIntent().getStringExtra("IP");
		channel = this.getIntent().getIntExtra("CHANNEL", 0);
		quality = this.getIntent().getIntExtra("QUALITY", 0);
		
		btIniciar = (Button)findViewById(R.id.btIniciar);
		btSalir = (Button) findViewById(R.id.btSalir);

		configSurfaceView();
		configSession();
		configRTSPClient();
	}
	
	// Config SurfaceView
	public void configSurfaceView(){
		mSurfaceView = (SurfaceView)findViewById(R.id.surfaceView);
		mHolder = mSurfaceView.getHolder();
		mHolder.addCallback(this);
	}
	
	public void configRTSPClient(){
		
		mRTSPClient = new RtspClient();
		mRTSPClient.setSession(mSession);
		mRTSPClient.setCallback(this);

		mRTSPClient.setCredentials(USER, PASS);
		mRTSPClient.setServerAddress(ip, 1935);
		mRTSPClient.setStreamPath("/live/canal" + channel);
		mRTSPClient.startStream();
	}
	
	public void configSession(){

		// Values by default
		int resX = 352;
		int resY = 288;
		int frameRate = 30;
		int bitRate = 300000;
		
		switch(quality){
			case 1: // Low
				resX = 352;
				resY = 288;
				frameRate = 30;
				bitRate = 300000;
				break;
			case 2: // Mid
				resX = 480;
				resY = 320;
				frameRate = 30;
				bitRate = 450000;
				break;
			case 3: // High
				resX = 800;
				resY = 480;
				frameRate = 30;
				bitRate = 600000;
				break;
		}

		mSession = SessionBuilder.getInstance()
		        .setCallback((Callback) this)
		        .setSurfaceView(mSurfaceView)
		        .setPreviewOrientation(90)
		        .setContext(getApplicationContext())
		        .setAudioEncoder(SessionBuilder.AUDIO_AAC)
		        .setAudioQuality(new AudioQuality(8000, 16000))
		        .setVideoEncoder(SessionBuilder.VIDEO_H264)
		        .setVideoQuality(new VideoQuality(resX, resY, frameRate, bitRate))
		                .build();
	}

	// ********************************
	// SurfaceHolder.Callback interface
	// ********************************
	
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		mSession.startPreview();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		mRTSPClient.stopStream();
	}
	
	@Override
	public void onBitrateUpdate(long bitrate) {
	    // Informs you of the bandwidth consumption of the streams
        Log.d(DEBUG,"Bitrate: " + bitrate);
		
	}

	@Override
	public void onSessionError(int reason, int streamType, Exception e) {
        Log.e(DEBUG, "An error occured", e);
	}

	@Override
	public void onPreviewStarted() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSessionConfigured() {
        Log.d(DEBUG,"Preview configured.");
        // Once the stream is configured, you can get a SDP formated session description
        // that you can send to the receiver of the stream.
        // For example, to receive the stream in VLC, store the session description in a .sdp file
        // and open it with VLC while streming.
        Log.e(DEBUG, mSession.getSessionDescription());
        //mSession.start();
		
	}

	@Override
	public void onSessionStarted() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSessionStopped() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void onRtspUpdate(int message, Exception exception) {
		// TODO Auto-generated method stub
	}
	
	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    mRTSPClient.release();
	    mSession.release();
	    mSurfaceView.getHolder().removeCallback(this);
	}

}