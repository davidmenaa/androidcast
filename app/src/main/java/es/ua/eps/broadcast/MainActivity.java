package es.ua.eps.broadcast;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;

public class MainActivity extends ActionBarActivity {

	EditText addressIPEditText;
	Spinner channelsSpinner;
	RadioGroup selectQualityRadio;
	Button initBroadcast;
	String ip;
	int channel;
	int selectedQuality;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        addressIPEditText = (EditText) findViewById(R.id.editText1);
        channelsSpinner = (Spinner) findViewById(R.id.spinner1);
        selectQualityRadio = (RadioGroup) findViewById(R.id.radioGroup1);
        
        channel = 1; //by default
        
        // Configure choice channels
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        channelsSpinner.setAdapter(adapter);
        
        loadListeners();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.action_about:
                Intent intent_about = new Intent(this,AboutActivity.class);
                startActivity(intent_about);
                break;

            case R.id.start:
                ip = addressIPEditText.getText().toString();
                Intent intent_onAir = new Intent(this, OnAirActivity.class);
                intent_onAir.putExtra("IP", ip);
                intent_onAir.putExtra("CHANNEL", channel);
                intent_onAir.putExtra("QUALITY", selectedQuality);
                startActivityForResult(intent_onAir, 1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    
    // Configure listeners
    public void loadListeners(){
    	
    	// Radio button group listener
        this.selectQualityRadio.setOnCheckedChangeListener(new OnCheckedChangeListener() 
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
    	
            	if(checkedId == R.id.radio0){
            		selectedQuality = 1;
            	}
            	else if(checkedId == R.id.radio1){
            		selectedQuality = 2;
            	}
            	else if(checkedId == R.id.radio2){
            		selectedQuality = 3;
            	}
            }
        });

    	// Spinner listener
    	channelsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
    		public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
    			channel = channelsSpinner.getSelectedItemPosition() + 1;
    		}

    		public void onNothingSelected(AdapterView<?> arg0) {
      
    		}
    	});
    }
    
}